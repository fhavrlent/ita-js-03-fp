import React from 'react';
import {Link} from 'react-router-dom';
import {ContactsService} from '../../Service/contact-service';

export class ContactDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContact();
    }

    componentWillReceiveProps(nextProps) {
        this.loadContact();
    }

    async loadContact() {
        const contactDetail = await ContactsService.getContact(this.props.match.params.id);
        this.setState({
            data: contactDetail
        });
    }

    handleDelete() {
        const sureDelete = window.confirm(`Sure delete ${this.state.data.name} ?`);
        if (sureDelete) {
            this.deleteContact();
        }
    }

    async deleteContact() {
        await ContactsService.delete(this.state.data.id);
        this.props.history.push('/contact');
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Contact detail for id: {this.state.data.id}</h1>
                </div>
                <div className="row main-contents">
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>Nama:</h3>
                                <p>{this.state.data.name}</p>
                            </div>
                            <div className="col-xs-12">
                                <h3>Phone:</h3>
                                <p>{this.state.data.phone}</p>
                            </div>
                            <div className="col-xs-12">
                                <h3>Address:</h3>
                                <p>{this.state.data.address}</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>Note:</h3>
                                <p>{this.state.data.note}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4 col-sm-offset-1">
                        <Link className="btn btn-info" to={`/contact/edit/${this.state.data.id}`}>
                            Edit
                        </Link>
                        <button onClick={this.handleDelete.bind(this)} className="btn btn-danger main-btn">Delete
                        </button>
                    </div>
                </div>

            </div>
        )
    }
};

