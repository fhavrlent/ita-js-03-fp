import React from "react";
import {ContactsService} from '../../Service/contact-service';
import {ContactForm} from './contact-form';

export class ContactAdd extends React.Component {

    handleCreate(newData) {
        this.create(newData);
    }

    async create(newData) {
        await ContactsService.create(newData);
        this.props.history.push('/contact');
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Add contact</h1>
                </div>
                <ContactForm handleFormSubmit={this.handleCreate.bind(this)}/>
            </div>
        );
    }
}

