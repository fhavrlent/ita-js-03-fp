import React from 'react';

export class ContactForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.handleFormSubmit(this.state.data);
    }

    handleChange(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    render() {
        return (
            <div className="row main-contents">
                <div className="col-sm-12">
                    <form onSubmit={this.handleSubmit.bind(this)} className="form-horizontal">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="inputName" className="col-sm-2 control-label">Name</label>
                                <div className="col-sm-10">
                                    <input name="name" autoFocus={true} type="text" className="form-control"
                                           id="inputName"
                                           placeholder="Name" value={this.state.data.name || ''}
                                           onChange={this.handleChange.bind(this)}
                                    />
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="inputPhone" className="col-sm-2 control-label">Phone</label>
                                <div className="col-sm-10">
                                    <input name="phone" type="text" className="form-control" id="inputPhone"
                                           placeholder="Phone number" value={this.state.data.phone || ''}
                                           onChange={this.handleChange.bind(this)}/>
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="inputAdress" className="col-sm-2 control-label">Adress</label>
                                <div className="col-sm-10">
                                    <input name="address" type="text" className="form-control" id="inputAdress"
                                           placeholder="Address" value={this.state.data.address || ''}
                                           onChange={this.handleChange.bind(this)}/>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="inputNote" className="col-sm-2 control-label">Note</label>
                                <div className="col-sm-10">
                                        <textarea name="note" id="inputNote" className="form-control" rows="3"
                                                  placeholder="Some note" value={this.state.data.note || ''}
                                                  onChange={this.handleChange.bind(this)}></textarea>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-12">
                            <div className="form-group">
                                <div className="col-lg-offset-1 col-lg-10">
                                    <button className="btn btn-warning" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        )
    }
};