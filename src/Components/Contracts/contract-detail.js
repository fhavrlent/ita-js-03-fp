import React from 'react';
import {Link} from 'react-router-dom';
import {ContractsService} from '../../Service/contract-service';

export class ContractDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContract();
    }

    componentWillReceiveProps(nextProps) {
        this.loadContract();
    }

    async loadContract() {
        const contractDetail = await ContractsService.getContract(this.props.match.params.id);
        this.setState({
            data: contractDetail
        });
    }

    handleDelete() {
        const sureDelete = window.confirm(`Sure delete ${this.state.data.name} ?`);
        if (sureDelete) {
            this.deleteContract();
        }
    }

    async deleteContract() {
        await ContractsService.delete(this.state.data.id);
        this.props.history.push('/contract');
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Contact detail for id: {this.state.data.id}</h1>
                </div>
                <div className="row main-contents">
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>Nama:</h3>
                                <p>{this.state.data.name}</p>
                            </div>
                            <div className="col-xs-12">
                                <h3>Price:</h3>
                                <p>{this.state.data.price}</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>Note:</h3>
                                <p>{this.state.data.note}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4 col-sm-offset-1">
                        <Link className="btn btn-info" to={`/contract/edit/${this.state.data.id}`}>
                            Edit
                        </Link>
                        <button onClick={this.handleDelete.bind(this)} className="btn btn-danger main-btn">Delete
                        </button>
                    </div>
                </div>

            </div>
        )
    }
};

