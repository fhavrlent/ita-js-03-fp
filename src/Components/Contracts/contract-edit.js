import React from 'react';
import {ContractsService} from '../../Service/contract-service';
import {ContactsService} from '../../Service/contact-service';
import {ContractForm} from './contract-form';

export class ContractEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contactData: []
        }
    }

    componentWillMount() {
        this.loadContract();
    }

    async loadContract() {
        const editContract = await ContractsService.getContract(this.props.match.params.id);
        const contact = await ContactsService.getContacts();
        this.setState({
            data: editContract,
            contactData: contact
        })
    }

    handleEdit() {
        this.updateContract();
    }

    async updateContract() {
        await ContractsService.update(this.state.data);
        this.props.history.push(`/contract/detail/${this.state.data.id}`);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Edit contact</h1>
                </div>
                <ContractForm contact={this.state.contactData} handleFormSubmit={this.handleEdit.bind(this)}
                              data={this.state.data}/>
            </div>
        );
    }
}
