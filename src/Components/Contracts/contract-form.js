import React from 'react';

export class ContractForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            contactData: [],
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data,
            contactData: nextProps.contact
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.handleFormSubmit(this.state.data);
    }

    handleSelectChange(event) {
        this.state.contactData[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    handleChange(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    render() {

        return (
            <div className="row main-contents">
                <div className="col-sm-12">
                    <form onSubmit={this.handleSubmit.bind(this)} className="form-horizontal">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="inputName" className="col-sm-2 control-label">Name</label>
                                <div className="col-sm-10">
                                    <input name="name" autoFocus={true} type="text" className="form-control"
                                           id="inputName"
                                           placeholder="Name" value={this.state.data.name || ''}
                                           onChange={this.handleChange.bind(this)}
                                    />
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="inputcontact" className="col-sm-2 control-label">Contact:</label>
                                <div className="col-sm-10">
                                    <select className="form-control" name="contact_name"
                                            value={this.state.contactData.contact_name}
                                            onChange={this.handleSelectChange.bind(this)}>
                                        {this.state.contactData.map(c => <option key={c.id}
                                                                                 value={c.id}>{c.name}</option>)}
                                    </select>
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="inputPhone" className="col-sm-2 control-label">Price</label>
                                <div className="col-sm-10">
                                    <input name="price" type="text" className="form-control" id="inputPrice"
                                           placeholder="Price" value={this.state.data.price || ''}
                                           onChange={this.handleChange.bind(this)}/>
                                </div>
                            </div>

                        </div>

                        <div className="col-sm-6">
                            <div className="form-group">
                                <label htmlFor="inputNote" className="col-sm-2 control-label">Note</label>
                                <div className="col-sm-10">
                                        <textarea name="note" id="inputNote" className="form-control" rows="3"
                                                  placeholder="Some note" value={this.state.data.note || ''}
                                                  onChange={this.handleChange.bind(this)}></textarea>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-12">
                            <div className="form-group">
                                <div className="col-lg-offset-1 col-lg-10">
                                    <button className="btn btn-warning" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        )
    }
}
;