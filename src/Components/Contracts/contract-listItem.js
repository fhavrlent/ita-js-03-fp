import React from "react";
import {Link} from 'react-router-dom';

export class ContractListItem extends React.Component {

    render() {
        return (
            <tr>
                <td>
                    <Link to={this.props.url}>{this.props.name}</Link>
                </td>
                <td>{this.props.price}</td>
                <td>{this.props.note}</td>
            </tr>
        );
    };
}
