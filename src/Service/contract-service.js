import _ from 'lodash'
import $http from 'axios'

export class ContractsService {

    static async getContracts() {
        return _.values((await $http.get('http://localhost:5000/contracts')).data)
    }

    static async getContract(id) {
        return (await $http.get(`http://localhost:5000/contracts/${id}`)).data
    }

    static async create(data) {
        return (await $http.post(`http://localhost:5000/contracts`, data))
    }

    static async update(data) {
        return (await $http.post(`http://localhost:5000/contracts/:id`, data))
    }

    static async delete(id) {
        return (await $http.delete(`http://localhost:5000/contracts/${id}`))
    }
}