PGDMP     
    *                u           ita-js    9.6.3    9.6.3     f           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            g           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            h           1262    16394    ita-js    DATABASE     �   CREATE DATABASE "ita-js" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Slovak_Slovakia.1250' LC_CTYPE = 'Slovak_Slovakia.1250';
    DROP DATABASE "ita-js";
          
   marossmrek    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            i           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            j           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16395    contact    TABLE     r   CREATE TABLE contact (
    id integer NOT NULL,
    name text,
    phone text,
    address text,
    note text
);
    DROP TABLE public.contact;
       public      
   marossmrek    false    3            �            1259    16398    contact_id_seq    SEQUENCE     p   CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.contact_id_seq;
       public    
   marossmrek    false    185    3            k           0    0    contact_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE contact_id_seq OWNED BY contact.id;
            public    
   marossmrek    false    186            �            1259    16409    contract    TABLE     d   CREATE TABLE contract (
    id integer NOT NULL,
    name text,
    price integer,
    note text
);
    DROP TABLE public.contract;
       public      
   marossmrek    false    3            �            1259    16412    contract_id_seq    SEQUENCE     q   CREATE SEQUENCE contract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.contract_id_seq;
       public    
   marossmrek    false    187    3            l           0    0    contract_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE contract_id_seq OWNED BY contract.id;
            public    
   marossmrek    false    188            �            1259    24628    user    TABLE     g   CREATE TABLE "user" (
    id integer NOT NULL,
    username text,
    email text,
    password text
);
    DROP TABLE public."user";
       public      
   marossmrek    false    3            �            1259    24631    user_id_seq    SEQUENCE     m   CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public    
   marossmrek    false    3    189            m           0    0    user_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE user_id_seq OWNED BY "user".id;
            public    
   marossmrek    false    190            �           2604    16400 
   contact id    DEFAULT     Z   ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);
 9   ALTER TABLE public.contact ALTER COLUMN id DROP DEFAULT;
       public    
   marossmrek    false    186    185            �           2604    16414    contract id    DEFAULT     \   ALTER TABLE ONLY contract ALTER COLUMN id SET DEFAULT nextval('contract_id_seq'::regclass);
 :   ALTER TABLE public.contract ALTER COLUMN id DROP DEFAULT;
       public    
   marossmrek    false    188    187            �           2604    24633    user id    DEFAULT     V   ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public    
   marossmrek    false    190    189            ^          0    16395    contact 
   TABLE DATA               :   COPY contact (id, name, phone, address, note) FROM stdin;
    public    
   marossmrek    false    185   �       n           0    0    contact_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('contact_id_seq', 5, true);
            public    
   marossmrek    false    186            `          0    16409    contract 
   TABLE DATA               2   COPY contract (id, name, price, note) FROM stdin;
    public    
   marossmrek    false    187          o           0    0    contract_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('contract_id_seq', 5, true);
            public    
   marossmrek    false    188            b          0    24628    user 
   TABLE DATA               8   COPY "user" (id, username, email, password) FROM stdin;
    public    
   marossmrek    false    189   >       p           0    0    user_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('user_id_seq', 10, true);
            public    
   marossmrek    false    190            �           2606    16408    contact contact_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.contact DROP CONSTRAINT contact_pkey;
       public      
   marossmrek    false    185    185            �           2606    16422    contract contract_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY contract
    ADD CONSTRAINT contract_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.contract DROP CONSTRAINT contract_pkey;
       public      
   marossmrek    false    187    187            �           2606    24641    user user_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public      
   marossmrek    false    189    189            ^   P   x�3��K-W�K�K�46�4�����K�N)�2��M,�/N�4�44�43717���/KT�)M�/�KT034�L)�4�=... #��      `   )   x�3�������NLL�44 μԲ��\���T�=... �N	�      b   E  x�}�[o�@���w�y�*�V��ʵ�(��4#
#w�ן�''i���ag?}��G����h1��^��A\��^8��#E��|�������2U6�E�K=;Ŵ��Pg��fXקy��Hu'��o��>�?_ь�#��oY2w;Ȃ�b˻�v���s嘘���u۹�ƃp�{>\;��ſ2O�Y��	��l�����?C��� ��j�5QM9j��M2����\�����]�r�2U�f7I,Ko�f�i�KO4�4�1`�F��#mʛ�.�F9ыO�$�Ux��n�E�z�)5����TCmy"aZ5�rMW�ᘱ��x���3����H����͇F&vbp��|^h����pkp��Ҹ�����)<}ܒT�(}�zy�1�����H���Mb�]��X?��O1�T�8�Z�롷ôL��L�_[��!P�8���ڒ�7V `�������3hպ�fĎ��5~��ՈV.��i�btR��M�>5 ��j�2BT���1����pͱ_L9��~�s�3�Bk���0	}�W w���X:׃�\KC	C���yd�ǽW� M��"     